package main

import (
	"context"
	"github.com/joho/godotenv"
	"github.com/rs/zerolog/log"
	"net/http"
	"os"
	"os/signal"
	_ "parser/docs"
	"parser/internal/app/handler"
	"parser/internal/app/repository"
	"parser/internal/app/service"
	"time"
)

// @title           Vacancies Parser + User Authorization
// @version         1.1
// @description     REST API server
// @host      localhost:8081

// @securityDefinitions.apiKey ApiKeyAuth
// @in header
// @name Authorization

func main() {

	var repo service.IRepository
	var err error

	// загружаем переменные окружения из файла .env в корневой директории проекта
	err = godotenv.Load()
	if err != nil {
		log.Fatal().Msg("Error loading .env file")
	}

	// Getenv извлекает значение переменной среды, названной по ключу
	dbType := os.Getenv("DBDRIVER")

	switch dbType {
	case "postgres":
		repo, err = repository.NewPostgresDB()
		if err != nil {
			log.Fatal().Err(err)
		}
	case "mongodb":
		repo, err = repository.NewMongoDB()
		if err != nil {
			log.Fatal().Err(err)
		}
	case "RAM":
		repo, err = repository.NewMemoryDB()
		if err != nil {
			log.Fatal().Err(err)
		}
	default:
		log.Fatal().Msg("Unknown database type")
	}

	// создаем кэшированный репозиторий
	cachedRepo := repository.NewCachedRepository(repo)
	// передаем репозиторий в конструктор сервиса
	serv := service.NewService(cachedRepo)
	// передаем сервис в конструктор контроллера
	hand := handler.NewHandler(serv)

	// запускаем сервер
	port := ":8081"
	server := http.Server{
		Addr:           port,
		Handler:        hand.InitRoutes(),
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}

	go func() {
		log.Info().Msgf("Server starting at port %v", port)
		err := server.ListenAndServe()
		if err != nil && err != http.ErrServerClosed {
			log.Fatal().Msg("Server stopped")
		}
	}()

	// ожидание сигнала
	quit := make(chan os.Signal, 1)
	// регистрируем обработчик для сигнала os.Interrupt
	signal.Notify(quit, os.Interrupt)
	// блокируем выполнение программы, пока не будет получен сигнал os. Interrupt
	<-quit
	log.Info().Msg("Shutdown Server")

	// тайм-аут завершения
	// создаем контекст ctx с таймаутом в 5 секунд и функцию cancel, которая может быть использована для отмены контекста
	// Если операция не завершится в течение указанного времени, контекст будет отменен и все горутины, связанные с этим контекстом, будут завершены.
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := server.Shutdown(ctx); err != nil {
		log.Fatal().Msgf("Server shutdown failed, %v", err)
	}
	defer func(server *http.Server) {
		err := server.Close()
		if err != nil {

		}
	}(&server)

	log.Info().Msg("Server exiting")

}
