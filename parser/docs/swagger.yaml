definitions:
  model.AuthResponse:
    properties:
      data:
        $ref: '#/definitions/model.LoginData'
      error_code:
        type: integer
      success:
        type: boolean
    type: object
  model.ChangePasswordRequest:
    properties:
      new_password:
        type: string
      old_password:
        type: string
    type: object
  model.Data:
    properties:
      message:
        type: string
      user:
        $ref: '#/definitions/model.User'
    type: object
  model.Identifier:
    properties:
      name:
        type: string
      value:
        type: string
    type: object
  model.LoginData:
    properties:
      access_token:
        type: string
      message:
        type: string
      refresh_token:
        type: string
    type: object
  model.LoginRequest:
    properties:
      email:
        type: string
      password:
        type: string
    type: object
  model.ProfileResponse:
    properties:
      data:
        $ref: '#/definitions/model.Data'
      error_code:
        type: integer
      success:
        type: boolean
    type: object
  model.RegisterRequest:
    properties:
      email:
        type: string
      password:
        type: string
      retype_password:
        type: string
    type: object
  model.RegisterResponse:
    properties:
      data:
        $ref: '#/definitions/model.Data'
      error_code:
        type: integer
      success:
        type: boolean
    type: object
  model.SwaggerID:
    properties:
      id:
        default: 1
        type: integer
    type: object
  model.SwaggerQuery:
    properties:
      query:
        type: string
    type: object
  model.User:
    properties:
      email:
        type: string
      email_verified:
        type: boolean
      id:
        type: integer
      name:
        type: string
      phone:
        type: string
      phone_verified:
        type: boolean
      role:
        type: integer
      verified:
        type: boolean
    type: object
  model.Vacancy:
    properties:
      datePosted:
        type: string
      description:
        type: string
      employmentType:
        type: string
      id:
        type: integer
      identifier:
        $ref: '#/definitions/model.Identifier'
      title:
        type: string
      validThrough:
        type: string
    type: object
  model.VerifyRequest:
    properties:
      email:
        type: string
      hash:
        type: string
    type: object
host: localhost:8081
info:
  contact: {}
  description: REST API server
  title: Vacancies Parser + User Authorization
  version: "1.1"
paths:
  /auth/login:
    post:
      consumes:
      - application/json
      description: Login user
      parameters:
      - description: JSON object
        in: body
        name: input
        required: true
        schema:
          $ref: '#/definitions/model.LoginRequest'
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            items:
              $ref: '#/definitions/model.AuthResponse'
            type: array
        "400":
          description: Bad Request
        "500":
          description: Internal Server Error
      summary: Login
      tags:
      - auth
  /auth/refresh:
    post:
      consumes:
      - application/json
      description: Refresh token
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            items:
              $ref: '#/definitions/model.AuthResponse'
            type: array
        "500":
          description: Internal Server Error
      summary: Refresh
      tags:
      - auth
  /auth/register:
    post:
      consumes:
      - application/json
      description: Register new user
      parameters:
      - description: JSON object
        in: body
        name: input
        required: true
        schema:
          $ref: '#/definitions/model.RegisterRequest'
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            items:
              $ref: '#/definitions/model.RegisterResponse'
            type: array
        "400":
          description: Bad Request
        "500":
          description: Internal Server Error
      summary: Register
      tags:
      - auth
  /auth/verify:
    post:
      consumes:
      - application/json
      description: Verify user
      parameters:
      - description: JSON object
        in: body
        name: input
        required: true
        schema:
          $ref: '#/definitions/model.VerifyRequest'
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            items:
              $ref: '#/definitions/model.AuthResponse'
            type: array
        "400":
          description: Bad Request
        "500":
          description: Internal Server Error
      summary: Verify
      tags:
      - auth
  /delete:
    delete:
      consumes:
      - application/json
      description: Delete vacancy by ID
      parameters:
      - description: JSON object
        in: body
        name: input
        required: true
        schema:
          $ref: '#/definitions/model.SwaggerID'
      produces:
      - application/json
      responses:
        "200":
          description: OK
        "404":
          description: Not Found
        "500":
          description: Internal Server Error
      security:
      - ApiKeyAuth: []
      summary: Delete vacancy
      tags:
      - vacancy
  /get:
    get:
      consumes:
      - application/json
      description: Get vacancies by ID
      parameters:
      - description: JSON object
        in: body
        name: input
        required: true
        schema:
          $ref: '#/definitions/model.SwaggerID'
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            $ref: '#/definitions/model.Vacancy'
        "400":
          description: Bad Request
        "404":
          description: Not Found
        "500":
          description: Internal Server Error
      security:
      - ApiKeyAuth: []
      summary: Get by ID
      tags:
      - vacancy
  /list:
    get:
      consumes:
      - application/json
      description: Get a list of all vacancies
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            items:
              $ref: '#/definitions/model.Vacancy'
            type: array
        "500":
          description: Internal Server Error
      security:
      - ApiKeyAuth: []
      summary: Get all vacancies
      tags:
      - vacancy
  /search:
    post:
      consumes:
      - application/json
      description: Search and parse vacancies
      parameters:
      - description: JSON object
        in: body
        name: input
        required: true
        schema:
          $ref: '#/definitions/model.SwaggerQuery'
      produces:
      - application/json
      responses:
        "200":
          description: OK
        "400":
          description: Bad Request
        "500":
          description: Internal Server Error
      security:
      - ApiKeyAuth: []
      summary: Search and parsing
      tags:
      - vacancy
  /user/changePassword:
    get:
      consumes:
      - application/json
      description: ChangePassword of profile
      parameters:
      - description: JSON object
        in: body
        name: input
        required: true
        schema:
          $ref: '#/definitions/model.ChangePasswordRequest'
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            items:
              $ref: '#/definitions/model.ProfileResponse'
            type: array
        "400":
          description: Bad Request
        "500":
          description: Internal Server Error
      summary: ChangePassword
      tags:
      - user
  /user/profile:
    get:
      consumes:
      - application/json
      description: Profile get info
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            items:
              $ref: '#/definitions/model.ProfileResponse'
            type: array
        "500":
          description: Internal Server Error
      summary: Profile
      tags:
      - user
securityDefinitions:
  ApiKeyAuth:
    in: header
    name: Authorization
    type: apiKey
swagger: "2.0"
