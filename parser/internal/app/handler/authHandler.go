package handler

import (
	"encoding/json"
	"github.com/rs/zerolog/log"
	"net/http"
	"parser/internal/app/model"
	"time"
)

// Register godoc
// @Summary      Register
// @Description  Register new user
// @Tags         auth
// @Accept       json
// @Produce      json
// @Param   input   body   model.RegisterRequest   true   "JSON object"
// @Failure      400
// @Failure      500
// @Success      200 {object} []model.RegisterResponse
// @Router       /auth/register [post]
func (h *Handler) Register(w http.ResponseWriter, r *http.Request) {
	var req model.RegisterRequest
	var resp model.RegisterResponse

	//декодируем запрос
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	// передаем структуру в слой сервиса для отправки в другой микросервис
	jsonResp, err := h.service.Register(req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	// декодируем полученный ответ
	err = json.NewDecoder(jsonResp.Body).Decode(&resp)
	jsonResp.Body.Close()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	// отправляем ошибку в случае неудачной регистрации
	if resp.Success != true {
		http.Error(w, "unsuccessful registration", http.StatusInternalServerError)
		return
	}

	// отправляем data и 200 если успешно
	err = json.NewEncoder(w).Encode(resp.Data.Message)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusOK)
}

// Login godoc
// @Summary      Login
// @Description  Login user
// @Tags         auth
// @Accept       json
// @Produce      json
// @Param   input   body   model.LoginRequest   true   "JSON object"
// @Failure      400
// @Failure      500
// @Success      200 {object} []model.AuthResponse
// @Router       /auth/login [post]
func (h *Handler) Login(w http.ResponseWriter, r *http.Request) {
	var req model.LoginRequest
	var resp model.AuthResponse

	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	jsonResp, err := h.service.Login(req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	err = json.NewDecoder(jsonResp.Body).Decode(&resp)
	jsonResp.Body.Close()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if resp.Success != true {
		http.Error(w, "unsuccessful login", http.StatusInternalServerError)
		return
	}

	//устанавливаем куки
	AccessTokenCookie := &http.Cookie{
		Name:     "access_token",
		Value:    resp.Data.AccessToken,
		Path:     "/",
		Domain:   "localhost",
		Expires:  time.Now().Add(1 * time.Hour),
		HttpOnly: true,
		Secure:   false,
		SameSite: http.SameSiteStrictMode,
	}
	http.SetCookie(w, AccessTokenCookie)

	RefreshTokenCookie := &http.Cookie{
		Name:     "refresh_token",
		Value:    resp.Data.RefreshToken,
		Path:     "/",
		Domain:   "localhost",
		Expires:  time.Now().Add(1 * time.Hour),
		HttpOnly: true,
		Secure:   false,
		SameSite: http.SameSiteStrictMode,
	}
	http.SetCookie(w, RefreshTokenCookie)

	// отправляем data и 200 если успешно
	err = json.NewEncoder(w).Encode(resp.Data)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusOK)
}

// Refresh godoc
// @Summary      Refresh
// @Description  Refresh token
// @Tags         auth
// @Accept       json
// @Produce      json
// @Failure      500
// @Success      200 {object} []model.AuthResponse
// @Router       /auth/refresh [post]
func (h *Handler) Refresh(w http.ResponseWriter, r *http.Request) {
	var resp model.AuthResponse

	// берем токен из куки
	cookie, err := r.Cookie("refresh_token")
	if err != nil {
		log.Err(err).Msg("get cookie error")
	}
	token := "Bearer " + cookie.Value

	jsonResp, err := h.service.Refresh(token)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	err = json.NewDecoder(jsonResp.Body).Decode(&resp)
	jsonResp.Body.Close()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if resp.Success != true {
		http.Error(w, "unsuccessful refresh", http.StatusInternalServerError)
		return
	}

	err = json.NewEncoder(w).Encode(resp.Data)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusOK)
}

// Verify godoc
// @Summary      Verify
// @Description  Verify user
// @Tags         auth
// @Accept       json
// @Produce      json
// @Param   input   body   model.VerifyRequest   true   "JSON object"
// @Failure      400
// @Failure      500
// @Success      200 {object} []model.AuthResponse
// @Router       /auth/verify [post]
func (h *Handler) Verify(w http.ResponseWriter, r *http.Request) {
	var req model.VerifyRequest
	var resp model.AuthResponse

	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	jsonResp, err := h.service.Verify(req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	// декодируем полученный ответ
	err = json.NewDecoder(jsonResp.Body).Decode(&resp)
	jsonResp.Body.Close()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	// отправляем ошибку в случае неудачной регистрации
	if resp.Success != true {
		http.Error(w, "unsuccessful verify", http.StatusInternalServerError)
		return
	}
	// отправляем data и 200 если успешно
	err = json.NewEncoder(w).Encode(resp.Data.Message)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusOK)
}
