package handler

import (
	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/golang-jwt/jwt/v4"
	"github.com/rs/zerolog/log"
	httpSwagger "github.com/swaggo/http-swagger"
	"net/http"
	_ "parser/docs"
)

const AccessSecret = "11111111111111111111111111111111111199999999999999999991111111111111111111111111111111111119999999999999999999"

func (h *Handler) InitRoutes() *chi.Mux {
	r := chi.NewRouter()
	r.Use(middleware.Logger)

	r.Route("/swagger", func(r chi.Router) {
		r.Get("/*", httpSwagger.Handler(
			httpSwagger.URL("http://localhost:8080/swagger/doc.json")))
	})

	r.Route("/vacancy", func(r chi.Router) {
		r.Use(CheckAccessToken)

		r.Post("/search", h.Search)
		r.Get("/get", h.GetByID)
		r.Delete("/delete", h.Delete)
		r.Get("/list", h.GetAll)
	})

	r.Route("/auth", func(r chi.Router) {
		r.Post("/register", h.Register)
		r.Post("/login", h.Login)
		r.Post("/refresh", h.Refresh)
		r.Post("/verify", h.Verify)
	})

	r.Route("/user", func(r chi.Router) {
		r.Get("/profile", h.Profile)
		r.Get("/changePassword", h.ChangePassword)
	})

	return r
}

func CheckAccessToken(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		//проверяем заголовок "Authorization" в запросе, что он содержит токен типа Bearer
		/*header := r.Header.Get("Authorization")
		headerParts := strings.Split(header, " ")
		if len(headerParts) != 2 && headerParts[0] != "Bearer" {
			http.Error(w, "token error", http.StatusUnauthorized)
			return
		}
		*/

		// берем токен из куки
		cookie, err := r.Cookie("access_token")
		if err != nil {
			log.Err(err).Msg("get cookie error")
		}

		//проверяем валидность токена
		token, err := jwt.Parse(cookie.Value, func(token *jwt.Token) (interface{}, error) {
			return []byte(AccessSecret), nil
		})
		if err != nil || !token.Valid {
			http.Error(w, "not valid token", http.StatusUnauthorized)
			return
		}

		next.ServeHTTP(w, r)
	})
}
