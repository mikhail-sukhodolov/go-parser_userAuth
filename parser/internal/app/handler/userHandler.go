package handler

import (
	"encoding/json"
	"github.com/rs/zerolog/log"
	"net/http"
	"parser/internal/app/model"
)

// Profile godoc
// @Summary      Profile
// @Description  Profile get info
// @Tags         user
// @Accept       json
// @Produce      json
// @Failure      500
// @Success      200 {object} []model.ProfileResponse
// @Router       /user/profile [get]
func (h *Handler) Profile(w http.ResponseWriter, r *http.Request) {
	var resp model.ProfileResponse

	// берем токен из куки
	cookie, err := r.Cookie("access_token")
	if err != nil {
		log.Err(err).Msg("get cookie error")
	}
	token := "Bearer " + cookie.Value

	jsonResp, err := h.service.Profile(token)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	err = json.NewDecoder(jsonResp.Body).Decode(&resp)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if resp.Success != true {
		http.Error(w, "unsuccessful get profile", http.StatusInternalServerError)
		return
	}

	err = json.NewEncoder(w).Encode(resp.Data)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusOK)
}

// ChangePassword godoc
// @Summary      ChangePassword
// @Description  ChangePassword of profile
// @Tags         user
// @Accept       json
// @Produce      json
// @Param   input   body   model.ChangePasswordRequest   true   "JSON object"
// @Failure      400
// @Failure      500
// @Success      200 {object} []model.ProfileResponse
// @Router       /user/changePassword [get]
func (h *Handler) ChangePassword(w http.ResponseWriter, r *http.Request) {
	var req model.ChangePasswordRequest
	var resp model.ProfileResponse

	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// берем токен из куки
	cookie, err := r.Cookie("access_token")
	if err != nil {
		log.Err(err).Msg("get cookie error")
	}
	token := "Bearer " + cookie.Value

	jsonResp, err := h.service.ChangePassword(token, req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	err = json.NewDecoder(jsonResp.Body).Decode(&resp)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if resp.Success != true {
		http.Error(w, "unsuccessful change password", http.StatusInternalServerError)
		return
	}

	err = json.NewEncoder(w).Encode(resp.Data.Message)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusOK)
}
