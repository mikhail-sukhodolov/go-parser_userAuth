package model

import "encoding/json"

func UnmarshalVacancy(data []byte) (Vacancy, error) {
	var r Vacancy
	err := json.Unmarshal(data, &r)
	return r, err
}

type SwaggerID struct {
	ID int `json:"id" default:"1"`
}

type SwaggerQuery struct {
	Query string `json:"query"`
}

type Vacancy struct {
	ID             int `json:"id" db:"id"`
	Identifier     `json:"identifier" bson:"identifier"`
	Title          string `json:"title" db:"title" bson:"title"`
	Description    string `json:"description" db:"description" bson:"description"`
	DatePosted     string `json:"datePosted" db:"date_posted" bson:"date_posted"`
	ValidThrough   string `json:"validThrough" db:"valid_through" bson:"valid_through"`
	EmploymentType string `json:"employmentType" db:"employment_type" bson:"employment_type"`
}

type Identifier struct {
	Value string `json:"value" db:"vacancy_id" bson:"vacancy_id"`
	Name  string `json:"name" db:"organization" bson:"organization"`
}

type RegisterRequest struct {
	Email          string `json:"email"`
	Password       string `json:"password"`
	RetypePassword string `json:"retype_password"`
}

type RegisterResponse struct {
	Success   bool `json:"success"`
	ErrorCode int  `json:"error_code,omitempty"`
	Data      Data `json:"data"`
}

type LoginRequest struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

type AuthResponse struct {
	Success   bool      `json:"success"`
	ErrorCode int       `json:"error_code,omitempty"`
	Data      LoginData `json:"data"`
}

type LoginData struct {
	AccessToken  string `json:"access_token"`
	RefreshToken string `json:"refresh_token"`
	Message      string `json:"message"`
}

type VerifyRequest struct {
	Email string `json:"email"`
	Hash  string `json:"hash"`
}

type ProfileResponse struct {
	Success   bool `json:"success"`
	ErrorCode int  `json:"error_code,omitempty"`
	Data      Data `json:"data"`
}

type Data struct {
	Message string `json:"message,omitempty"`
	User    User   `json:"user,omitempty"`
}

type User struct {
	ID            int    `json:"id"`
	Name          string `json:"name"`
	Phone         string `json:"phone"`
	Email         string `json:"email"`
	Password      string `json:"-"`
	Role          int    `json:"role"`
	Verified      bool   `json:"verified"`
	EmailVerified bool   `json:"email_verified"`
	PhoneVerified bool   `json:"phone_verified"`
}

type ChangePasswordRequest struct {
	OldPassword string `json:"old_password"`
	NewPassword string `json:"new_password"`
}

type ChangePasswordResponse struct {
	Success   bool `json:"success"`
	ErrorCode int  `json:"error_code"`
}
