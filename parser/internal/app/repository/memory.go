package repository

import (
	"errors"
	"parser/internal/app/model"
	"sync"
)

type MemoryDB struct {
	data  map[string]*model.Vacancy
	count int
	sync.RWMutex
}

func NewMemoryDB() (*MemoryDB, error) {
	return &MemoryDB{
		data:  make(map[string]*model.Vacancy),
		count: 1,
	}, nil
}

func (r *MemoryDB) AddVacancy(vacancy model.Vacancy) error {
	r.Lock()
	defer r.Unlock()

	identifier := vacancy.Identifier.Value
	if _, ok := r.data[identifier]; !ok {
		r.data[identifier] = &vacancy
		r.data[identifier].ID = r.count
		r.count++
		return nil
	}
	return errors.New("vacancy with ID already exists")
}

func (r *MemoryDB) GetByID(id int) (model.Vacancy, error) {
	r.RLock()
	defer r.RUnlock()

	for _, value := range r.data {
		if value.ID == id {
			return *value, nil
		}
	}
	return model.Vacancy{}, errors.New("NotFound")
}

func (r *MemoryDB) GetList() ([]model.Vacancy, error) {
	r.RLock()
	defer r.RUnlock()

	var vacancies []model.Vacancy
	for _, value := range r.data {
		vacancies = append(vacancies, *value)
	}
	return vacancies, nil
}

func (r *MemoryDB) Delete(id int) error {
	r.Lock()
	defer r.Unlock()

	for _, vacancy := range r.data {
		if vacancy.ID == id {
			delete(r.data, vacancy.Identifier.Value)
			return nil
		}
	}
	return errors.New("NotFound")
}
