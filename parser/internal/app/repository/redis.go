package repository

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/redis/go-redis/v9"
	"github.com/rs/zerolog/log"
	"parser/internal/app/model"
	"parser/internal/app/service"
)

type CachedRepository struct {
	repository service.IRepository // Исходный репозиторий
	cache      *redis.Client       // Клиент Redis
}

func NewCachedRepository(repository service.IRepository) *CachedRepository {
	return &CachedRepository{
		repository: repository,
		cache: redis.NewClient(&redis.Options{
			Addr:     "redisdb:6380",
			Password: "", // Пароль Redis (если требуется)
			DB:       0,  // Номер базы данных Redis
		}),
	}
}

func (c *CachedRepository) AddVacancy(vacancy model.Vacancy) error {
	return c.repository.AddVacancy(vacancy)
}

func (c *CachedRepository) GetByID(id int) (model.Vacancy, error) {
	var vacancy model.Vacancy
	// ключ для проверки наличия в кэше данного id
	cacheId := fmt.Sprintf("vacancy:%v", id)
	// проверяем наличие записи в кэше

	// пробуем взять запись с указанным id
	result, err := c.cache.Get(context.Background(), cacheId).Bytes()
	// если удачно(нет ошибки), то запись в кэше существует
	if err == nil {
		// делаем декодирование из json в структуру и возвращаем ее
		if err := json.Unmarshal(result, &vacancy); err == nil {
			log.Info().Msg("get from cache - successfully")
			return vacancy, nil
		}
	}
	// если неудачно, то логируем ошибку и вызываем метод репозитория
	log.Error().Err(err).Msg("error cache")
	vacancy, err = c.repository.GetByID(id)
	if err != nil {
		return model.Vacancy{}, err
	}

	// при получении структуры кодируем ее для добавления в кэш
	jsonVacancy, err := json.Marshal(vacancy)
	if err != nil {
		log.Error().Err(err).Msg("marshal cache error")
	} else {

		// добавляем запись в кэш
		err := c.cache.Set(context.Background(), cacheId, jsonVacancy, 0).Err()
		if err != nil {
			log.Error().Err(err).Msg("added cache error")
		}
	}
	return vacancy, nil

}

func (c *CachedRepository) GetList() ([]model.Vacancy, error) {
	return c.repository.GetList()
}

func (c *CachedRepository) Delete(id int) error {
	cachedId := fmt.Sprintf("vacancy:%v", id)
	err := c.cache.Del(context.Background(), cachedId).Err()
	if err != nil {
		log.Error().Err(err).Msg("cache delete error")
	}
	return c.repository.Delete(id)
}
