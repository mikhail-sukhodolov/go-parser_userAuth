package service

import (
	"bytes"
	"encoding/json"
	"net/http"
	"parser/internal/app/model"
)

type AuthService interface {
	Register(model.RegisterRequest) (*http.Response, error)
	Login(model.LoginRequest) (*http.Response, error)
	Refresh(string) (*http.Response, error)
	Verify(model.VerifyRequest) (*http.Response, error)
}

func (s *Service) Register(request model.RegisterRequest) (*http.Response, error) {
	// кодируем структуру запроса
	jsonData, err := json.Marshal(request)
	if err != nil {
		return nil, err
	}

	// подготавливаем запрос в микросервис
	req, err := http.NewRequest("POST", "http://sampleapi:8080/api/1/auth/register", bytes.NewBuffer(jsonData))
	if err != nil {
		return nil, err
	}

	// отправляем запрос
	resp, err := MicroserviceRequest(req)
	if err != nil {
		return nil, err
	}
	return resp, nil
}

func (s *Service) Login(request model.LoginRequest) (*http.Response, error) {
	jsonData, err := json.Marshal(request)
	if err != nil {
		return nil, err
	}

	req, err := http.NewRequest("POST", "http://sampleapi:8080/api/1/auth/login", bytes.NewBuffer(jsonData))
	if err != nil {
		return nil, err
	}

	resp, err := MicroserviceRequest(req)
	if err != nil {
		return nil, err
	}
	return resp, nil
}

func (s *Service) Refresh(currentToken string) (*http.Response, error) {
	req, err := http.NewRequest("POST", "http://sampleapi:8080/api/1/auth/refresh", nil)
	if err != nil {
		return nil, err
	}

	req.Header.Set("Authorization", currentToken)

	resp, err := MicroserviceRequest(req)
	if err != nil {
		return nil, err
	}
	return resp, nil
}

func (s *Service) Verify(request model.VerifyRequest) (*http.Response, error) {
	jsonData, err := json.Marshal(request)
	if err != nil {
		return nil, err
	}

	req, err := http.NewRequest("POST", "http://sampleapi:8080/api/1/auth/verify", bytes.NewBuffer(jsonData))
	if err != nil {
		return nil, err
	}

	resp, err := MicroserviceRequest(req)
	if err != nil {
		return nil, err
	}
	return resp, nil
}
