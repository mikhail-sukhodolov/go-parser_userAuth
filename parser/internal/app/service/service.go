package service

import (
	"net/http"
	"parser/internal/app/model"
)

type IService interface {
	VacancyService
	AuthService
	UserService
}

type IRepository interface {
	AddVacancy(vacancy model.Vacancy) error
	GetByID(id int) (model.Vacancy, error)
	GetList() ([]model.Vacancy, error)
	Delete(id int) error
}

type Service struct {
	repo IRepository
}

func NewService(repo IRepository) *Service {
	return &Service{repo: repo}
}

func MicroserviceRequest(req *http.Request) (*http.Response, error) {
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	return resp, nil
}
