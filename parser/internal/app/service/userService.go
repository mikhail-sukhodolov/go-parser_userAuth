package service

import (
	"bytes"
	"encoding/json"
	"net/http"
	"parser/internal/app/model"
)

type UserService interface {
	Profile(string) (*http.Response, error)
	ChangePassword(string, model.ChangePasswordRequest) (*http.Response, error)
}

func (s *Service) Profile(currentToken string) (*http.Response, error) {
	req, err := http.NewRequest("GET", "http://sampleapi:8080/api/1/user/profile/", nil)
	if err != nil {
		return nil, err
	}

	req.Header.Set("Authorization", currentToken)

	resp, err := MicroserviceRequest(req)
	if err != nil {
		return nil, err
	}
	return resp, nil
}

func (s *Service) ChangePassword(currentToken string, request model.ChangePasswordRequest) (*http.Response, error) {
	jsonData, err := json.Marshal(request)
	if err != nil {
		return nil, err
	}

	req, err := http.NewRequest("GET", "http://sampleapi:8080/api/1/user/changePassword", bytes.NewBuffer(jsonData))
	if err != nil {
		return nil, err
	}

	req.Header.Set("Authorization", currentToken)

	resp, err := MicroserviceRequest(req)
	if err != nil {
		return nil, err
	}
	return resp, nil
}
