package service

import (
	"parser/internal/app/model"
)

type VacancyService interface {
	NewVacancy(vacancy model.Vacancy) error
	GetVacancyByID(id int) (model.Vacancy, error)
	GetVacancyList() ([]model.Vacancy, error)
	DeleteVacancy(id int) error
}

func (s *Service) NewVacancy(vacancy model.Vacancy) error {
	return s.repo.AddVacancy(vacancy)
}

func (s *Service) GetVacancyByID(id int) (model.Vacancy, error) {
	return s.repo.GetByID(id)
}

func (s *Service) GetVacancyList() ([]model.Vacancy, error) {
	return s.repo.GetList()
}

func (s *Service) DeleteVacancy(id int) error {
	return s.repo.Delete(id)
}
