package component

import (
	"github.com/ptflp/godecoder"
	"go.uber.org/zap"
	"userAuth/config"
	"userAuth/internal/infrastructure/responder"
	"userAuth/internal/infrastructure/service"
	"userAuth/internal/infrastructure/tools/cryptography"
)

type Components struct {
	Conf         config.AppConf
	Notify       service.Notifier
	TokenManager cryptography.TokenManager
	Responder    responder.Responder
	Decoder      godecoder.Decoder
	Logger       *zap.Logger
	Hash         cryptography.Hasher
}

func NewComponents(conf config.AppConf, notify service.Notifier, tokenManager cryptography.TokenManager, responder responder.Responder, decoder godecoder.Decoder, hash cryptography.Hasher, logger *zap.Logger) *Components {
	return &Components{Conf: conf, Notify: notify, TokenManager: tokenManager, Responder: responder, Decoder: decoder, Hash: hash, Logger: logger}
}
