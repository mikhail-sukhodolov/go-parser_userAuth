package modules

import (
	"userAuth/internal/infrastructure/component"
	aservice "userAuth/internal/modules/auth/service"
	uservice "userAuth/internal/modules/user/service"
	"userAuth/internal/storages"
)

type Services struct {
	User uservice.Userer
	Auth aservice.Auther
}

func NewServices(storages *storages.Storages, components *component.Components) *Services {
	userService := uservice.NewUserService(storages.User, components.Logger)
	return &Services{
		User: userService,
		Auth: aservice.NewAuth(userService, storages.Verify, components),
	}
}
