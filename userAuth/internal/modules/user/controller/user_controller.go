package controller

import (
	"github.com/ptflp/godecoder"
	"net/http"
	"userAuth/internal/infrastructure/component"
	"userAuth/internal/infrastructure/errors"
	"userAuth/internal/infrastructure/handlers"
	"userAuth/internal/infrastructure/responder"
	"userAuth/internal/modules/user/service"
)

type Userer interface {
	Profile(http.ResponseWriter, *http.Request)
	ChangePassword(http.ResponseWriter, *http.Request)
}

type User struct {
	service service.Userer
	responder.Responder
	godecoder.Decoder
}

func NewUser(service service.Userer, components *component.Components) Userer {
	return &User{service: service, Responder: components.Responder, Decoder: components.Decoder}
}

func (u *User) Profile(w http.ResponseWriter, r *http.Request) {

	claims, err := handlers.ExtractUser(r)
	if err != nil {
		u.ErrorBadRequest(w, err)
		return
	}

	out := u.service.GetByID(r.Context(), service.GetByIDIn{UserID: claims.ID})
	if out.ErrorCode != errors.NoError {
		u.OutputJSON(w, ProfileResponse{
			ErrorCode: out.ErrorCode,
			Data: Data{
				Message: "retrieving user error",
			},
		})
		return
	}

	u.OutputJSON(w, ProfileResponse{
		Success:   true,
		ErrorCode: out.ErrorCode,
		Data: Data{
			User: *out.User,
		},
	})
}

func (u *User) ChangePassword(w http.ResponseWriter, r *http.Request) {
	var req service.ChangePasswordIn
	var resp service.ChangePasswordOut

	// декодируем тело запроса
	err := u.Decode(r.Body, &req)
	if err != nil {
		u.ErrorBadRequest(w, err)
		return
	}
	// вытягиваем данные пользователя из сессии
	claims, err := handlers.ExtractUser(r)
	if err != nil {
		u.ErrorBadRequest(w, err)
		return
	}

	// присваиваем id в запрос
	req.UserID = claims.ID

	// отправляем данные в слой сервиса
	resp = u.service.ChangePassword(r.Context(), req)
	if resp.ErrorCode != errors.NoError {
		msg := "change password error"
		if resp.ErrorCode == errors.AuthServiceWrongPasswordErr {
			msg = "unknown old password"
		}
		u.OutputJSON(w, ProfileResponse{
			Success:   false,
			ErrorCode: resp.ErrorCode,
			Data: Data{
				Message: msg,
			},
		})
		return
	}

	u.OutputJSON(w, ProfileResponse{
		Success: true,
		Data: Data{
			Message: "successful password change",
		},
	})
}
