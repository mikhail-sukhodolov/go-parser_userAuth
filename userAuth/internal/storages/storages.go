package storages

import (
	"userAuth/internal/db/adapter"
	"userAuth/internal/infrastructure/cache"
	vstorage "userAuth/internal/modules/auth/storage"
	ustorage "userAuth/internal/modules/user/storage"
)

type Storages struct {
	User   ustorage.Userer
	Verify vstorage.Verifier
}

func NewStorages(sqlAdapter *adapter.SQLAdapter, cache cache.Cache) *Storages {
	return &Storages{
		User:   ustorage.NewUserStorage(sqlAdapter, cache),
		Verify: vstorage.NewEmailVerify(sqlAdapter),
	}
}
